﻿using Microsoft.AspNetCore.Identity;

namespace ShopGameIdentityServer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
