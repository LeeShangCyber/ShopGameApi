using ShopGameApi.Data;
using ShopGameApi.Core;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Persistence.Repositories;
using System.Threading.Tasks;
namespace ShopGameApi.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly ShopGameApiDBContext _context;

        public IGameRepository Games { get; private set; }
        public ICompanyRepository Companies { get; private set; }
        public ICategoryRepository Categories { get; private set; }
        public IRatingRepository Ratings { get; private set;}
        public ICategoryGameRepository CategoryGame { get; private set; }
        public UnitOfWork(ShopGameApiDBContext context)
        {
            _context = context;
            Games = new GameRepository(_context);
            Companies = new CompanyRepository(_context);
            Categories = new CategoryRepository(_context);
            Ratings = new RatingRepository(_context);
            CategoryGame = new CategoryGameRepository(_context);
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}