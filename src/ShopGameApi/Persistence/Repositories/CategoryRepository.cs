using ShopGameApi.Models;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ShopGameApi.Persistence.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ShopGameApiDBContext context) : base(context)
        {

        }

        public ShopGameApiDBContext ShopGameApiDBContext 
        {
            get { return _context as ShopGameApiDBContext; }
        }

        public async Task<IEnumerable<Category>> GetCategoriesWithGamesAsync()
        {
            return await ShopGameApiDBContext.Categories
                .Include(c => c.CategoryGame)
                .ThenInclude(cg => cg.Category)
                .ToListAsync();
        }

    }
}