// using ShopGameApi.Models;
// using ShopGameApi.Core.Repositories;
// using ShopGameApi.Data;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;

// namespace ShopGameApi.Persistence.Repositories
// {
//     public class UserRepository : Repository<User>, IUserRepository
//     {
//         public UserRepository(ShopGameApiDBContext context) : base (context)
//         {

//         }

//         public ShopGameApiDBContext ShopGameApiDBContext
//         {
//             get { return _context as ShopGameApiDBContext; }
//         }

//         public async Task<IEnumerable<User>> GetUsersWithGamesAsync()
//         {
//             return await ShopGameApiDBContext.Users.Include(g => g.UserGame).ThenInclude(ug => ug.Game).ToListAsync();
//         }

//     }
// }