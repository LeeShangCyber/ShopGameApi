using ShopGameApi.Models;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ShopGameApi.Persistence.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(ShopGameApiDBContext context) : base(context)
        {

        }

        public ShopGameApiDBContext ShopGameApiDBContext
        {
            get { return _context as ShopGameApiDBContext; }
        }

        public int RatingGet()
        {
            return 1;
        }

    }
}