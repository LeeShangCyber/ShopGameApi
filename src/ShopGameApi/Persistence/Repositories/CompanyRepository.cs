using ShopGameApi.Models;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ShopGameApi.Persistence.Repositories
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(ShopGameApiDBContext context) : base(context)
        {

        }

        public ShopGameApiDBContext ShopGameApiDBContext
        {
            get { return _context as ShopGameApiDBContext; }
        }

        public async Task<IEnumerable<Company>> GetCompaniesWithGamesAsync()
        {
            return await ShopGameApiDBContext.Companies.Include(c => c.Games).ToListAsync();
        }

    }
}