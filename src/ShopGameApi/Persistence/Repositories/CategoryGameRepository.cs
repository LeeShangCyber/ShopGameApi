using ShopGameApi.Models;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ShopGameApi.Persistence.Repositories
{
    public class CategoryGameRepository : Repository<CategoryGame>, ICategoryGameRepository
    {
        public CategoryGameRepository(ShopGameApiDBContext context) : base(context)
        {

        }

        public ShopGameApiDBContext ShopGameApiDBContext 
        {
            get { return _context as ShopGameApiDBContext; }
        }

    }
}