using ShopGameApi.Models;
using ShopGameApi.Core.Repositories;
using ShopGameApi.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ShopGameApi.Persistence.Repositories
{
    public class GameRepository : Repository<Game>, IGameRepository
    {
        public GameRepository(ShopGameApiDBContext context) : base(context)
        {

        }

        public ShopGameApiDBContext ShopGameApiDBContext
        {
            get { return _context as ShopGameApiDBContext; }
        }

        public async Task<IEnumerable<Game>> GetGamesWithRatingAsync()
        {
            return await ShopGameApiDBContext.Games.Include(g => g.Rating).ToListAsync();
        }

        public async Task<IEnumerable<Game>> GetGamesWithCompaniesAsync()
        {
            return await ShopGameApiDBContext.Games.Include(g => g.Company).ToListAsync();
        }

    }
}