using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.DTOs;
using ShopGameApi.Queries.Company;
using ShopGameApi.Command.Company;
using Mapster;
using MediatR;

namespace ShopGameApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CompanyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<List<CompanyDTO>> GetCompaniesAsync() 
        { 
            return await _mediator.Send(new GetAllCompaniesQuery());
        }

        [HttpGet("{id}")]
        public async Task<List<GameDTOs>> GetGameOfCompanyAsync(int id)
        {
            return await _mediator.Send(new GetGamesOfCompanyQuery
            {
                Id = id,
            });
        }

        [HttpPost("{id}/AddGame")]
        public async Task<bool> AddGameByCompanyAsync(int id, [FromBody] AddGameByCompanyCommand command)
        {
            command.idCompany = id;

            return await _mediator.Send(command);
        }

        [HttpPost("AddCompany")]
        public async Task<bool> PostAddCompanyAsync(AddCompanyCommand command)
        {
            return await _mediator.Send(command);
        }

    }
}