// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Identity;
// using Microsoft.AspNetCore.Authorization;
// using ShopGameApi.Models;
// using ShopGameApi.Data;

// namespace ShopGameApi.Controllers
// {
//     [ApiController]
//     [Route("api/administrator")]
//     [Authorize (Roles = "Administrator")]
//     public class AdministratorController : Controller
//     {
//         public ShopGameApiDBContext _context;

//         public AdministratorController(ShopGameApiDBContext context)
//         {
//             _context = context;
//         }

//         [HttpPost("signup")]
//         public async Task<IActionResult> PostSignUpAdminAsync(User user)
//         {
//             User result = await _context.Users.FirstOrDefaultAsync<User>(u => u.Name == user.Name);

//             if (result != null)
//             {
//                 return BadRequest("User name is established!");
//             }

//             user.Password = new PasswordHasher<User>().HashPassword(user, user.Password);

//             user.UserRole = UserRole.Administrator;

//             await _context.Users.AddAsync(user);
//             await _context.SaveChangesAsync();

//             return Ok(user);
//         }
//     }
// }