using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ShopGameApi.Models;
using ShopGameApi.Data;
using ShopGameApi.DTOs;
using ShopGameApi.Queries.Category;
using ShopGameApi.Command.Category;
using Mapster;
using MediatR;

namespace ShopGameApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {

        private readonly ShopGameApiDBContext _context;
        private readonly IMediator _mediator;

        public CategoryController(ShopGameApiDBContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<List<CategoryDTOs>> GetCategories() 
        {
            GetAllCategoriesQuery query = new GetAllCategoriesQuery(); // chuyển vào send

            return  await _mediator.Send(query);;
        }

        [HttpPost("AddCategory")]
        public async Task<bool> PostAddCategory(AddCategoryCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> PostAddGameToCategory(int id, [FromBody]Game game)
        {
            _context.Categories.Include(c => c.CategoryGame).ThenInclude(cg => cg.Game);
            Category category = await _context.Categories.FindAsync(id);
            Game game48 = await _context.Games.FindAsync(game.GameId);
            
            if (category == null || game48 == null)
            {
                return BadRequest(new {error = "Category or Game is not Established!"} );
            }

            CategoryGame categoryGame = await _context.CategoryGame.FirstOrDefaultAsync<CategoryGame>(cg => (cg.GameId == game48.GameId && cg.GameId == category.CategoryId));

            if (categoryGame == null)
            {
                categoryGame = new CategoryGame
                {
                    GameId = game48.GameId,
                    CategoryId = category.CategoryId
                };

                await _context.CategoryGame.AddAsync(categoryGame);
                if (game48.CategoryGame == null) game48.CategoryGame = new List<CategoryGame>();
                if (category.CategoryGame == null) category.CategoryGame = new List<CategoryGame>();
                game48.CategoryGame.Add(categoryGame);
                category.CategoryGame.Add(categoryGame);
                await _context.SaveChangesAsync();
                return Ok(categoryGame);
            }

            return BadRequest(new { error = "Category had game" });
        }

    }
}