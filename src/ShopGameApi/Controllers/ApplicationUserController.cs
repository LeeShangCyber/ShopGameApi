using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.DTOs;
using Mapster;
namespace ShopGameApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ApplicationUserController : ControllerBase
    {
        private readonly ShopGameApiDBContext _context;
        private readonly IConfiguration _config;

        public ApplicationUserController(ShopGameApiDBContext context, IConfiguration configuration)
        {
            _context = context;
            _config = configuration;
        }

        [Authorize]
        [HttpGet]
        public async  Task<IActionResult> GetUserName()
        {
            List<Claim> claims = HttpContext.User.Claims.ToList();
            ApplicationUser user = await GetApplicationUser(claims);

            return Ok(user);
        }

        [Authorize]
        [HttpGet("AddGame/{id}")]
        public async Task<IActionResult> GetAddApplicationUserGame(int id)
        {

            _context.ApplicationUsers.Include(au => au.ApplicationUserGame).ThenInclude(ag => ag.Game).ToList();
            Game game = await _context.Games.FirstOrDefaultAsync(g => g.GameId == id);
            List<Claim> claims = HttpContext.User.Claims.ToList();
            ApplicationUser user = await GetApplicationUser(claims);

            if (user == null || game == null)
            {
                return BadRequest("Game/User Invalid!");
            }

            ApplicationUserGame applicationUserGame = await  _context.ApplicationUserGame.
                    FirstOrDefaultAsync(ag => (ag.ApplicationUserId == user.Id && ag.GameId == game.GameId));

            if (applicationUserGame != null)
            {
                return BadRequest(" User had game !");
            }

            applicationUserGame = new ApplicationUserGame
            {
                GameId = game.GameId,
                ApplicationUserId = user.Id,
            };

            if (user.ApplicationUserGame == null)
                user.ApplicationUserGame = new List<ApplicationUserGame>();
            if (game.ApplicationUserGame == null)
                game.ApplicationUserGame = new List<ApplicationUserGame>();

            user.ApplicationUserGame.Add(applicationUserGame);
            game.ApplicationUserGame.Add(applicationUserGame);

            await _context.SaveChangesAsync();

            return Ok(applicationUserGame);
        }

        [HttpGet("abcdgej")]
        public async  Task<ApplicationUser> GetApplicationUser(List<Claim> claims)
        {
            ApplicationUser applicationUser = null;
            
            Claim claim = new Claim("null", "null");
            
            foreach (Claim temp in claims)
            {
                if (temp.Type == ClaimTypes.NameIdentifier)
                {
                    claim = temp;
                }
            }
            
            applicationUser = await _context.ApplicationUsers
            .Include(au => au.ApplicationUserGame)
            .ThenInclude(ag => ag.Game)
            .FirstOrDefaultAsync(u => u.Id == claim.Value);

            return applicationUser;
        }

    }
}