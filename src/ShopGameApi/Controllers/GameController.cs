using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.DTOs;
using ShopGameApi.Queries.Game;
using Mapster;
using MediatR;

namespace ShopGameApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private readonly ShopGameApiDBContext _context;
        private readonly IConfiguration _config;

        private readonly IMediator _mediator;

        public GameController(ShopGameApiDBContext context, IConfiguration config, IMediator mediator)
        {
            _context = context;
            _config = config;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<List<GameDTOs>> GetListGames()
        {   
            var clock = System.Diagnostics.Stopwatch.StartNew();
            clock.Start();
            GetGamesQuery query = new GetGamesQuery();
            var result = await _mediator.Send(query);

            clock.Stop();
            Console.WriteLine("Using MediatR " + clock.ElapsedMilliseconds.ToString());
            return result;
        //     var clock = System.Diagnostics.Stopwatch.StartNew();
        //     clock.Start();
        //     var games = await _context.Games
        //     .AsNoTracking()
        //     .Include(g => g.Company)
        //     .Include(g => g.Rating)
        //     .Include(g => g.Rating)
        //     .Include(g => g.CategoryGame)
        //     .ThenInclude(cg => cg.Category).ToListAsync();

        //     List<GameDTOs> gameObjectJsons = new List<GameDTOs>();

        //    gameObjectJsons = games.Adapt<List<GameDTOs>>();
            
        //     clock.Stop();
        //     Console.WriteLine("Non-using MEdiatR " + clock.ElapsedMilliseconds.ToString());
        //     return gameObjectJsons;
        }

        [HttpPost("AddGame")]
        public async Task<IActionResult> PostAddGame(Game game)
        {

            Game result = await _context.Games.FirstOrDefaultAsync<Game>(g => g.Name == game.Name);

            if (result == null)
            {
                Company company = new Company();
                if (game.Company != null)
                    company = await _context.Companies.FirstOrDefaultAsync<Company>(c => c.Name == game.Company.Name);
                else
                {
                    return BadRequest(new { error = "Lack of Company of Game" });
                }

                if (company == null)
                {
                    await _context.Companies.AddAsync(game.Company);
                    await _context.SaveChangesAsync();
                    company = await _context.Companies.FirstOrDefaultAsync<Company>(c => c.Name == game.Company.Name);
                }       

            
                Rating rating = new Rating();

                rating.Quantity = 0;
                rating.Score = 0.0;

                game.Company = null;
                
                await _context.Ratings.AddAsync(rating);
                await _context.Games.AddAsync(game);
                game.Company = company;
                game.Rating = rating;
                await _context.SaveChangesAsync();

                return Ok(game);
            }

            return BadRequest(new { error = "Cannot create Game" });
        }

    }
}