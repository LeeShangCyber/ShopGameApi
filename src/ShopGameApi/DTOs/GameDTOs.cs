using System;
using System.Collections.Generic;

namespace ShopGameApi.DTOs
{
    public class GameDTOs
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime ReleasedTime { get; set; }
        public string Company { get; set; }
        public List<string> Category { get; set; }
        public double Score { get; set; } 
    }
}