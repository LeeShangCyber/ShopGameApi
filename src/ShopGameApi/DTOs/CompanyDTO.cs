using System.Collections.Generic;

namespace ShopGameApi.DTOs
{
    public class CompanyDTO
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public List<string> Games { get; set; }
    }
}