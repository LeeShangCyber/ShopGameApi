using System;
using System.Collections.Generic;

namespace ShopGameApi.DTOs
{
    public class CategoryDTOs
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public List<string> Games { get; set; }  
    }
}