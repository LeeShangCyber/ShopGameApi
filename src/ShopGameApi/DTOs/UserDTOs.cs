using System.Collections;
using System.Collections.Generic;
using ShopGameApi.Models;
namespace ShopGameApi.DTOs
{
    public class UserDTOs
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Games { get; set; }
    }
}