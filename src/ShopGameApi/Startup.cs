using System;
using System.Reflection;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.DTOs;
using ShopGameApi.Core;
using ShopGameApi.Persistence;
using ShopGameApi.PipelineBehaviors;
using Mapster;
using MediatR;
using FluentValidation;

namespace ShopGameApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ShopGameApiDBContext>(opt => opt.UseMySql(Configuration.GetConnectionString(nameof(ShopGameApiDBContext))));

            // services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            // .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,options => {
            //     options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            //     {
            //         ValidateIssuer = true,
            //         ValidateAudience = true,
            //         ValidateLifetime = true,
            //         ValidateIssuerSigningKey = true,
            //         ValidIssuer = Configuration[JWT.Issuer],
            //         ValidAudience = Configuration[JWT.Issuer],
            //         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration[JWT.Key]))
            //     };
            // });

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options => 
                {
                    options.Authority = "https://localhost:5001";
                    
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api1");
                });
            });

            services.AddSwaggerGen(
                c =>
                { 
                    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo 
                    { 
                        Title="Shop Game API", 
                        Version = "v1",
                        Description = "APIs for get Data from shop game",
                        TermsOfService =  new Uri("https://ShopGame.com"),
                        Contact = new Microsoft.OpenApi.Models.OpenApiContact
                        {
                            Name = "Lê Sang",
                            Email = "lesang541191309@gmail.com",
                            Url = new Uri("https://sang.com"), 
                        },
                        License = new Microsoft.OpenApi.Models.OpenApiLicense
                        {
                            Name = "Shop Game API LICX",
                            Url = new Uri("https://example.com"),
                        }
                    });

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);

                });

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddMediatR(typeof(Startup));

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));

            services.AddValidatorsFromAssembly(typeof(Startup).Assembly);

            services.AddControllers().AddNewtonsoftJson(options => {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            }).AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            TypeAdapterConfig<Game, GameDTOs>.NewConfig()
            .Map(dest => dest.Company, src => src.Company.Name)
            .Map(dest => dest.Score, src => src.Rating.Score)
            .Map(dest => dest.Category, src => src.CategoryGame.Select(cg => cg.Category.Name).ToList());

            TypeAdapterConfig<Category, CategoryDTOs>.NewConfig()
            .Map(dest => dest.Games, src => src.CategoryGame.Select(cg => cg.Game.Name).ToList());

            TypeAdapterConfig<User, UserDTOs>.NewConfig()
            .Map(dest => dest.Games, src => src.UserGame.Select(ug => ug.Game.Name).ToList());

            TypeAdapterConfig<Company, CompanyDTO>.NewConfig()
            .Map(dest => dest.Games, src => src.Games.Select(g => g.Name).ToList());

            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), @"swagger-ui")),
                RequestPath = new PathString("/swagger-ui")
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseSwagger();

            app.UseSwaggerUI(c => 
            {
                c.SwaggerEndpoint("v1/swagger.json", "Shop Game API V1");
                c.InjectStylesheet("/swagger-ui/custom.css");
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
