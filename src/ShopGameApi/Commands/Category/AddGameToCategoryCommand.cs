using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Core;
using ShopGameApi.Models;
using ShopGameApi.Data;
using MediatR;
using FluentValidation;

namespace ShopGameApi.Command.Category
{
    public class AddGameToCategoryCommand : IRequest<bool>
    {
        public int GameId { get; set; }
        public int CategoryId { get; set; }
    }

    public class AddGameToCategoryCommandValidator : AbstractValidator<AddGameToCategoryCommand>
    {
        public AddGameToCategoryCommandValidator(ShopGameApiDBContext context)
        {
            
        }
    }

    public class AddGameToCategoryCommandHandler : IRequestHandler<AddGameToCategoryCommand, bool>
    {

        public IUnitOfWork _unitOfWork; 
        public AddGameToCategoryCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddGameToCategoryCommand command, CancellationToken cancellationToken)
        {

            await _unitOfWork.Complete();
            return true;
        }
    }

}