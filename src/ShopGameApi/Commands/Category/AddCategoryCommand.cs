using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Core;
using ShopGameApi.Models;
using ShopGameApi.Data;
using MediatR;
using FluentValidation;

namespace ShopGameApi.Command.Category
{
    public class AddCategoryCommand : IRequest<bool>
    {
        public string Name { get; set; }
    }

    public class AddCategoryCommandValidator : AbstractValidator<AddCategoryCommand>
    {
        public AddCategoryCommandValidator(ShopGameApiDBContext context)
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(128)
            .MustAsync(async (name, cancellationToken) =>
            {
                return (await context.Categories.FirstOrDefaultAsync(c => c.Name == name)) == null;
            })
            .WithMessage("Category is exits");
        }
    }

    public class AddCategoryCommandHandler : IRequestHandler<AddCategoryCommand, bool>
    {

        public IUnitOfWork _unitOfWork; 
        public AddCategoryCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddCategoryCommand command, CancellationToken cancellationToken)
        {
            ShopGameApi.Models.Category category = new ShopGameApi.Models.Category
            {
                Name = command.Name,
            };
            _unitOfWork.Categories.Add(category);
            await _unitOfWork.Complete();
            return true;
        }
    }

}