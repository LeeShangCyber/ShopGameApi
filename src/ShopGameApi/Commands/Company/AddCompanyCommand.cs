using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.Core;
using MediatR;
using FluentValidation;
using Mapster;

namespace ShopGameApi.Command.Company
{
    public class AddCompanyCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public string Country { get; set; }
    }

    public class AddCompanyCommandValidator : AbstractValidator<AddCompanyCommand>
    {
        public AddCompanyCommandValidator(ShopGameApiDBContext context)
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(128)
            .MustAsync(async (name, cancellationToken) =>
            {
                return (await context.Companies.FirstOrDefaultAsync(c => c.Name == name)) == null;
            })
            .WithMessage("Company is exits");

            RuleFor(x => x.Country).NotEmpty().MaximumLength(128);
        }
    }

    public class AddCompanyCommandHandler : IRequestHandler<AddCompanyCommand, bool>
    {
        public IUnitOfWork _unitOfWork;

        public AddCompanyCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddCompanyCommand command, CancellationToken cancellation)
        {
            ShopGameApi.Models.Company company = new ShopGameApi.Models.Company
            {
                Name = command.Name,
                Country = command.Country,
            };
            _unitOfWork.Companies.Add(company);
            await _unitOfWork.Complete();
            return true;
        }
    }

}