using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.Models;
using ShopGameApi.Core;
using MediatR;
using FluentValidation;
using Mapster;

namespace ShopGameApi.Command.Company
{
    public class AddGameByCompanyCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime ReleasedTime { get; set; }
        public int idCompany { get; set; }
    }

    public class AddGameByCompanyCommandValidator : AbstractValidator<AddCompanyCommand>
    {
        public AddGameByCompanyCommandValidator(ShopGameApiDBContext context)
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(128)
            .MustAsync(async (name, cancellationToken) =>
            {
                return (await context.Games.FirstOrDefaultAsync(c => c.Name == name)) == null;
            })
            .WithMessage("Game is exits");

            RuleFor(x => x.Country).NotEmpty().MaximumLength(128);
        }
    }

    public class AddGameByCompanyCommandHandler : IRequestHandler<AddGameByCompanyCommand, bool>
    {
        public IUnitOfWork _unitOfWork;

        public AddGameByCompanyCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddGameByCompanyCommand command, CancellationToken cancellation)
        {
            ShopGameApi.Models.Game game = new ShopGameApi.Models.Game
            {
                Name = command.Name,
                Price = command.Price,
                ReleasedTime = command.ReleasedTime,
            };
            
            ShopGameApi.Models.Company company = await _unitOfWork.Companies.GetAsync(command.idCompany);

            Rating rating = new Rating();
            rating.Quantity = 0;
            rating.Score = 0;
            
            _unitOfWork.Ratings.Add(rating);

            game.Rating = rating;
            game.Company = company;
            
            _unitOfWork.Games.Add(game);
            await _unitOfWork.Complete();
            return true;
        }
    }

}