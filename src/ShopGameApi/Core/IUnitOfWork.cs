using ShopGameApi.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace ShopGameApi.Core
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Categories { get; }
        IGameRepository Games { get; }
        ICompanyRepository Companies { get; }
        IRatingRepository Ratings { get; }
        ICategoryGameRepository CategoryGame { get; }
        Task<int> Complete();
    } 
}