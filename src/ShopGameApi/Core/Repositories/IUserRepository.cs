using ShopGameApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopGameApi.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetUsersWithGamesAsync();
    }
}