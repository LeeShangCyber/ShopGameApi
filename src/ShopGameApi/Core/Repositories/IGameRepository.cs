using ShopGameApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopGameApi.Core.Repositories
{
    public interface IGameRepository : IRepository<Game>
    {
        Task<IEnumerable<Game>> GetGamesWithRatingAsync();
        Task<IEnumerable<Game>> GetGamesWithCompaniesAsync();
    }

}