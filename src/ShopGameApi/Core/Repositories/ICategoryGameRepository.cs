using ShopGameApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopGameApi.Core.Repositories
{
    public interface ICategoryGameRepository : IRepository<CategoryGame>
    {

    }
}