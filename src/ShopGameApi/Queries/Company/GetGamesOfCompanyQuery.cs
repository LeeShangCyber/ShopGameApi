using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.DTOs;
using MediatR;
using FluentValidation;
using Mapster;

namespace ShopGameApi.Queries.Company
{
    public class GetGamesOfCompanyQueryValidator : AbstractValidator<GetGamesOfCompanyQuery>
    {
        public GetGamesOfCompanyQueryValidator()
        {
            Console.WriteLine("test GetAllCompaniesQueryValidator");
        }
    }

    public class GetGamesOfCompanyHandler : IRequestHandler<GetGamesOfCompanyQuery, List<GameDTOs>>
    {

        protected readonly ShopGameApiDBContext _context;        

        public GetGamesOfCompanyHandler(ShopGameApiDBContext context)
        {
            _context = context;
        }

        public async Task<List<GameDTOs>> Handle(GetGamesOfCompanyQuery request, CancellationToken cancellationToken)
        {
            List<ShopGameApi.Models.Game> result = (await  _context.Games
            .Include(g => g.Company)
            .Include(g => g.Rating)
            .Include(g => g.CategoryGame)
            .ThenInclude(cg => cg.Category)
            .Where(g => g.Company.CompanyId == request.Id)
            .ToListAsync());

            List<GameDTOs> gameDTOs = new List<GameDTOs>();

            if (result != null)
                gameDTOs = result.Adapt<List<GameDTOs>>();
            return gameDTOs;
        }
    }

    public class GetGamesOfCompanyQuery : IRequest<List<GameDTOs>>
    {
        public int Id {get; set;}
    }
}