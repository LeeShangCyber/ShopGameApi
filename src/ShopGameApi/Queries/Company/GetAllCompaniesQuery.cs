using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.DTOs;
using MediatR;
using FluentValidation;
using Mapster;

namespace ShopGameApi.Queries.Company
{
    public class GetAllCompaniesQueryValidator : AbstractValidator<GetAllCompaniesQuery>
    {
        public GetAllCompaniesQueryValidator()
        {
            Console.WriteLine("test GetAllCompaniesQueryValidator");
        }
    }

    public class GetAllCompaniesQueryHandler : IRequestHandler<GetAllCompaniesQuery, List<CompanyDTO>>
    {

        protected readonly ShopGameApiDBContext _context;        

        public GetAllCompaniesQueryHandler(ShopGameApiDBContext context)
        {
            _context = context;
        }

        public async Task<List<CompanyDTO>> Handle(GetAllCompaniesQuery request, CancellationToken cancellationToken)
        {
            var result = (await  _context.Companies.AsNoTracking()
            .Include(c => c.Games)
            .ToListAsync())
            .Adapt<List<CompanyDTO>>();

            return result;
        }
    }

    public class GetAllCompaniesQuery : IRequest<List<CompanyDTO>>
    {

    }
}