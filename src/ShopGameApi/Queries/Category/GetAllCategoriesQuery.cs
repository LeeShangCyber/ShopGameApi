using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.DTOs;
using MediatR;
using FluentValidation;
using Mapster;

namespace ShopGameApi.Queries.Category
{

    public class GetAllCategoriesQueryValidator : AbstractValidator<GetAllCategoriesQuery>
    {
        public GetAllCategoriesQueryValidator()
        {
            Console.WriteLine("test GetAllCategoriesValidator");
        }
    }

    public class GetAllCategoriesQueryHandler : IRequestHandler<GetAllCategoriesQuery, List<CategoryDTOs>>
    {
        private readonly ShopGameApiDBContext _context;

        public GetAllCategoriesQueryHandler(ShopGameApiDBContext context)
        {
            _context = context;
        }

        public async Task<List<CategoryDTOs>> Handle(GetAllCategoriesQuery query, CancellationToken cancellationToken)
        {
            return (await _context.Categories.AsNoTracking().Include(c => c.CategoryGame)
            .ThenInclude(cg => cg.Game)
            .ToListAsync()).Adapt<List<CategoryDTOs>>();
        }

    }

    public class GetAllCategoriesQuery : IRequest<List<CategoryDTOs>>
    {

    }
}