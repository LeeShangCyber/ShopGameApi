using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using FluentValidation;
using MediatR;
using Mapster;
using Microsoft.EntityFrameworkCore;
using ShopGameApi.Data;
using ShopGameApi.DTOs;

namespace ShopGameApi.Queries.Game
{
    public class GetGamesQueryValidator : AbstractValidator<GetGamesQuery>
    {
        public GetGamesQueryValidator(ShopGameApiDBContext context)
        {
            Console.WriteLine("GetGamesQueryHandler");
        }
    }

    public class GetGamesQueryHandler : IRequestHandler<GetGamesQuery, List<GameDTOs>>
    {
        private readonly ShopGameApiDBContext _context;

        public GetGamesQueryHandler(ShopGameApiDBContext context)
        {
            _context = context;
        }

        public async Task<List<GameDTOs>> Handle(GetGamesQuery request, CancellationToken cancellationToken)
        {
            var games = await _context.Games
            .AsNoTracking()
            .Include(g => g.Company)
            .Include(g => g.Rating)
            .Include(g => g.Rating)
            .Include(g => g.CategoryGame)
            .ThenInclude(cg => cg.Category).ToListAsync();

            List<GameDTOs> gameObjectJsons = new List<GameDTOs>();

           gameObjectJsons = games.Adapt<List<GameDTOs>>();

           return gameObjectJsons;
        }   
    }

    public class GetGamesQuery : IRequest<List<GameDTOs>>
    {

        public Guid Id { get; set; }

    }
}