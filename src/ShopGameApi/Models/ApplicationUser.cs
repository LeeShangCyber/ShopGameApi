using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ShopGameApi.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base() {}
        [XmlIgnore]
        public IList<ApplicationUserGame> ApplicationUserGame { get; set; }

    }
}
