namespace ShopGameApi.Models
{
    public class ApplicationUserGame
    {
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}